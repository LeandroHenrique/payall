package com.killlegacy.payall.ui.main;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.killlegacy.payall.R;
import com.killlegacy.payall.api.Service;
import com.killlegacy.payall.model.Usuario;
import com.killlegacy.payall.ui.home.HomeActivity;
import com.killlegacy.payall.ui.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.et_email) TextInputEditText etEmail;

    @BindView(R.id.et_password) TextInputEditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    public void registrar(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }


    @OnClick(R.id.btn_signin)
    public void entrar(View view) {

        if (etEmail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Email vazio", Toast.LENGTH_SHORT).show();
        }

        if (etPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Senha vazia", Toast.LENGTH_SHORT).show();
        }

        login();
    }

    private void login() {

        startActivity(new Intent(MainActivity.this, HomeActivity.class));

//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();
//
//        new Retrofit.Builder()
//                .baseUrl(Service.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build()
//                .create(Service.class)
//                .login(new Usuario(etEmail.getText().toString(), etPassword.getText().toString()))
//                .enqueue(new Callback<String>() {
//
//                         @Override
//                         public void onResponse(Call<String> call, Response<String> response) {
//                             Log.d("", response.raw().request().url().toString());
//
//                             if (response == null || response.body() == null) {
//                                 // Toast.makeText(MainActivity.this, "Ocorreu uma falha na autenticação", Toast.LENGTH_LONG).show();
//                                 return;
//                             }
//
//                             Log.d("", "logado com sucesso");
//                             startActivity(new Intent(MainActivity.this, HomeActivity.class));
//
//                         }
//
//                         @Override
//                         public void onFailure(Call<String> call, Throwable t) {
//
//                             Log.d("AA", t.getMessage());
//                             Toast.makeText(MainActivity.this, "Falha na autenticação", Toast.LENGTH_LONG).show();
//                         }
//                 }
//        );
    }



}
