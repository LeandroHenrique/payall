package com.killlegacy.payall.ui.resume;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.killlegacy.payall.R;
import com.killlegacy.payall.model.Buyer;
import com.killlegacy.payall.model.StatusEnum;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResumeActivity extends AppCompatActivity {

    @BindView(R.id.rv_friends) RecyclerView rvFriends;

    @BindView(R.id.tv_value) TextView tvValue;

    private ResumeAdapter adapter;

    private ArrayList<Buyer> buyers;

    private float amount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Resumo</font>"));

        ArrayList<String> emails = (ArrayList<String>) getIntent().getSerializableExtra("FRIENDS");
        amount = getIntent().getExtras().getFloat("VALUE");

        tvValue.setText("R$ " + amount);

        buyers = new ArrayList<>();
        for (String email : emails) {
            String temp = String.format("%.2f", amount/(float)emails.size()).replaceAll(",", ".");
            buyers.add(new Buyer(email, Float.valueOf(temp)));
        }

        adapter = new ResumeAdapter(this, buyers);

        rvFriends.setAdapter(adapter);
        rvFriends.setNestedScrollingEnabled(false);

    }


    @OnClick(R.id.btn_conclude)
    public void concluirCompra(View view) {

        for (Buyer buyer : buyers) {
            buyer.setStatus(StatusEnum.LOADING);
        }


        adapter.notifyDataSetChanged();

        for (final Buyer buyer : buyers) {

            new Thread(){
                @Override
                public void run() {
                    try {
                        Thread.sleep(new Random().nextInt(10000));
                        buyer.setStatus(StatusEnum.SUCCESS);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }.start();

        }



//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();
//
//        new Retrofit.Builder()
//                .baseUrl(Service.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build()
//                .create(Service.class)
//                .doTransaction(new DataTransaction(buyers))
//                .enqueue(new Callback<String>() {
//
//                         @Override
//                         public void onResponse(Call<String> call, Response<String> response) {
//                             Log.d("", response.raw().request().url().toString());
//
//                             if (response == null || response.body() == null) {
//                                 return;
//
//                            }
//
//                             Toast.makeText(ResumeActivity.this, "Sucesso!!!", Toast.LENGTH_LONG).show();
//
//                         }
//
//                         @Override
//                         public void onFailure(Call<String> call, Throwable t) {
//                             Log.d("AA", t.getMessage());
//                             Toast.makeText(ResumeActivity.this, "Falha na transação", Toast.LENGTH_LONG).show();
//                         }
//                     }
//                );

    }

    @OnClick(R.id.btn_invoice)
    public void generateInvoice(View view) {
        Toast.makeText(ResumeActivity.this, "Not implemented yet.", Toast.LENGTH_SHORT).show();
    }



}
