package com.killlegacy.payall.ui.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.killlegacy.payall.R;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    @BindView(R.id.request_statement)
    Button requestStatement;

    @BindView(R.id.swipe_selector)
    SwipeSelector swipeSelector;

    @BindView(R.id.tv_name)
    TextView tvName;

    private QRCodeReaderView qrCodeReaderView;

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";


    private final int MY_CAMERA_PERMISSION = 1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        requestNameFromAPI();

        swipeSelector.setItems(
                new SwipeItem(0, "Elo 1", "xxxx xxxx xxxx 9028"),
                new SwipeItem(1, "Elo 2", "xxxx xxxx xxxx 2390")
        );


        //initQRCodeReader();
    }

    private void initQRCodeReader() {

//        qrCodeReaderView = findViewById(R.id.qrdecoderview);
//        qrCodeReaderView.setOnQRCodeReadListener(this);
//
//        // Use this function to enable/disable decoding
//        qrCodeReaderView.setQRDecodingEnabled(true);
//
//        // Use this function to change the autofocus interval (default is 5 secs)
//        qrCodeReaderView.setAutofocusInterval(2000L);
//
//        // Use this function to enable/disable Torch
//        qrCodeReaderView.setTorchEnabled(true);
//
//        // Use this function to set front camera preview
//        qrCodeReaderView.setFrontCamera();
//
//        // Use this function to set back camera preview
//        qrCodeReaderView.setBackCamera();
    }


    private void requestNameFromAPI() {

        tvName.setText("Tiago Sampa");

    }


    //product qr code mode
    public void scanQR(View v) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION);
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        boolean permission = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;

        switch (requestCode) {

            case MY_CAMERA_PERMISSION: {
                if (permission) {
                    startActivity(new Intent(this, QRCodeActivity.class));
                }
                return;
            }

        }
    }


}
