package com.killlegacy.payall.ui.register;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.killlegacy.payall.R;
import com.killlegacy.payall.api.Service;
import com.killlegacy.payall.model.Usuario;
import com.killlegacy.payall.ui.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.et_name) TextInputEditText etName;

    @BindView(R.id.et_email) TextInputEditText etEmail;

    @BindView(R.id.et_password) TextInputEditText etPassword;

    @BindView(R.id.et_cpf) TextInputEditText etCpf;

    @BindView(R.id.et_rg) TextInputEditText etRg;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.btn_register)
    public void registeUser() {

        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String cpf = etCpf.getText().toString();
        String rg = etRg.getText().toString();

        // valida campos
        if (name.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Nome vazio", Toast.LENGTH_LONG).show();
            return;
        } else if (email.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Email vazio", Toast.LENGTH_LONG).show();
            return;
        } else if (password.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Senha vazio", Toast.LENGTH_LONG).show();
            return;
        } else if (cpf.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "CPF vazio", Toast.LENGTH_LONG).show();
            return;
        } else if (rg.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "RG vazio", Toast.LENGTH_LONG).show();
            return;
        }

        Usuario user = new Usuario();
        user.setNome(name);
        user.setEmail(email);
        user.setPassword(password);
        user.setCpf(cpf);
        user.setRg(rg);


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        new Retrofit.Builder()
                .baseUrl(Service.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(Service.class)
                .registeUser(user)
                .enqueue(new Callback<String>() {

                     @Override
                     public void onResponse(Call<String> call, Response<String> response) {
                         Log.d("", response.raw().request().url().toString());

                         if (response == null || response.body() == null) {
                             Toast.makeText(RegisterActivity.this, "Erro: resposta vazia", Toast.LENGTH_LONG).show();
                             return;
                         }

                         Log.d(getClass().getSimpleName(), "logado com sucesso: " + response.body().toString());
                         startActivity(new Intent(RegisterActivity.this, HomeActivity.class));

                     }

                     @Override
                     public void onFailure(Call<String> call, Throwable t) {
                         Toast.makeText(RegisterActivity.this, "Falha no registro de usuário", Toast.LENGTH_LONG).show();
                     }
                }
        );

    }

}
