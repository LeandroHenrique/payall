package com.killlegacy.payall.ui.merchant;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.killlegacy.payall.R;

import java.util.List;

public class AddFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private LayoutInflater inflater;
    private List<String> friends;


    public AddFriendAdapter(Context context, List<String> friends) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.friends = friends;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.row_friend, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myHolder = (MyViewHolder) holder;
        myHolder.tvFriend.setText(friends.get(position));
    }


    @Override
    public int getItemCount() {
        return friends.size();
    }

    public void addFriend(String friend) {
        //friends.add(friend);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvFriend;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvFriend = itemView.findViewById(R.id.tv_name);
        }

    }



}
