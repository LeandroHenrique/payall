package com.killlegacy.payall.ui.merchant;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.killlegacy.payall.R;
import com.killlegacy.payall.ui.merchant.AddFriendAdapter;
import com.killlegacy.payall.ui.resume.ResumeActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MerchantActivity extends AppCompatActivity {


    @BindView(R.id.rv_friends) RecyclerView rvFriends;

    @BindView(R.id.te_friend) TextInputEditText teFriends;

    @BindView(R.id.tiet_value) TextInputEditText tietValue;

    private ArrayList<String> friends;

    private AddFriendAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Divisão de Conta</font>"));

        friends = new ArrayList<>();
        adapter = new AddFriendAdapter(getApplicationContext(), friends);

        rvFriends.setAdapter(adapter);
        rvFriends.setNestedScrollingEnabled(false);

        Log.d(getClass().getSimpleName(), "Chamei create");

    }


    @OnClick(R.id.btn_add_friend)
    public void addFriend(View view) {

        if (teFriends.getText().toString().isEmpty()) {
            Toast.makeText(this, "Email vazio", Toast.LENGTH_LONG).show();
            return;
        }

        friends.add(teFriends.getText().toString());
        adapter.notifyDataSetChanged();
        teFriends.setText("");

    }

    @OnClick(R.id.btn_save)
    public void goToResume(View view) {

        if (tietValue.getText().toString().isEmpty()) {
            Toast.makeText(this, "Valor vazio", Toast.LENGTH_SHORT).show();
            return;
        }

        // add myself to the list
        if (!friends.contains("myself@gmail.com")){
            friends.add("myself@gmail.com");
        }

        Log.d(getClass().getSimpleName(), "Chamei o add friend");
        Intent intent = new Intent(this, ResumeActivity.class);
        intent.putExtra("FRIENDS", friends);
        intent.putExtra("VALUE", Float.valueOf(tietValue.getText().toString()));

        startActivity(intent);
    }


}
