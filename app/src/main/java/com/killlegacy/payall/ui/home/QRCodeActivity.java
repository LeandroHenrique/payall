package com.killlegacy.payall.ui.home;

import android.content.Intent;
import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.killlegacy.payall.R;
import com.killlegacy.payall.ui.merchant.MerchantActivity;

public class QRCodeActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {


    private QRCodeReaderView qrCodeReaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        Log.d(getClass().getSimpleName(), "Iniciando as configs");
        initQRCodeReader();
    }

    private void initQRCodeReader() {

        qrCodeReaderView = findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);

        // Use this function to enable/disable decoding
        qrCodeReaderView.setQRDecodingEnabled(true);

        // Use this function to change the autofocus interval (default is 5 secs)
        qrCodeReaderView.setAutofocusInterval(2000L);

        // Use this function to enable/disable Torch
        qrCodeReaderView.setTorchEnabled(true);

        // Use this function to set front camera preview
        qrCodeReaderView.setFrontCamera();

        // Use this function to set back camera preview
        qrCodeReaderView.setBackCamera();


        Log.d(getClass().getSimpleName(), "Configuracoes setadas");
    }

        // Called when a QR is decoded
        // "text" : the text encoded in QR
        // "points" : points where QR control points are placed in View
        @Override
        public void onQRCodeRead(String text, PointF[] points) {
            Log.d(getClass().getSimpleName(), "Resultado: " + text);
            startActivity(new Intent(this, MerchantActivity.class));
        }

        @Override
        protected void onResume() {
            super.onResume();
            qrCodeReaderView.startCamera();
        }

        @Override
        protected void onPause() {
            super.onPause();
            qrCodeReaderView.stopCamera();
        }
}
