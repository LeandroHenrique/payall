package com.killlegacy.payall.ui.resume;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.killlegacy.payall.R;
import com.killlegacy.payall.model.Buyer;
import com.killlegacy.payall.model.StatusEnum;

import java.util.List;

public class ResumeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private LayoutInflater inflater;
    private List<Buyer> friends;


    public ResumeAdapter(Context context, List<Buyer> friends) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.friends = friends;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.row_friend_bill, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder myHolder = (MyViewHolder) holder;
        myHolder.tvFriend.setText(friends.get(position).getEmail());
        myHolder.etValue.setText("" + friends.get(position).getValue());

        if (friends.get(position).getStatus() == StatusEnum.LOADING) {
            myHolder.etValue.setVisibility(View.GONE);
            myHolder.pbSpin.setVisibility(View.VISIBLE);
        }

        if (friends.get(position).getStatus() == StatusEnum.SUCCESS) {
            myHolder.pbSpin.setVisibility(View.GONE);
            myHolder.ivResult.setImageResource(R.drawable.success);
            myHolder.ivResult.setVisibility(View.VISIBLE);
        }
        else if (friends.get(position).getStatus() == StatusEnum.FAIL) {
            myHolder.pbSpin.setVisibility(View.GONE);
            myHolder.ivResult.setImageResource(R.drawable.fail);
            myHolder.ivResult.setVisibility(View.VISIBLE);
        }
        
        verifyTransactionDone();

    }

    private void verifyTransactionDone() {

        int count = 0;
        for (Buyer buyer : friends) {
            if (buyer.getStatus() == StatusEnum.SUCCESS) {
                count++;
            }
        }

        if (count == friends.size()) {
            ((ResumeActivity) context).findViewById(R.id.btn_invoice).setVisibility(View.VISIBLE);
            ((ResumeActivity) context).findViewById(R.id.btn_conclude).setVisibility(View.GONE);
        }

    }


    @Override
    public int getItemCount() {
        return friends.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvFriend;

        EditText etValue;

        ProgressBar pbSpin;

        ImageView ivResult;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvFriend = itemView.findViewById(R.id.tv_name);
            etValue = itemView.findViewById(R.id.et_value);
            pbSpin = itemView.findViewById(R.id.progress_spinner);
            ivResult = itemView.findViewById(R.id.iv_result);
        }

    }



}
