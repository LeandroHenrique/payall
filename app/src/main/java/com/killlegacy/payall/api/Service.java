package com.killlegacy.payall.api;

import com.killlegacy.payall.model.DataTransaction;
import com.killlegacy.payall.model.Usuario;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Service {

    String BASE_URL = "http://10.5.3.174:9090/";

    @POST("usuario/autenticar")
    Call<String> login(@Body Usuario userData);

    @POST("usuario/cadastrar")
    Call<String> registeUser(@Body Usuario userData);

    @POST("authorization/add")
    Call<String> doTransaction(@Body DataTransaction dataTransaction);

}
