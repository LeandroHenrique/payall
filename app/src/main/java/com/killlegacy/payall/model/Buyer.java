package com.killlegacy.payall.model;

import java.io.Serializable;

public class Buyer implements Serializable  {

    private String email;

    private float value;

    private StatusEnum status;

    public Buyer(String email, float value) {
        this.email = email;
        this.value = value;
        status = StatusEnum.NEUTRAL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
