package com.killlegacy.payall.model;


import java.io.Serializable;

public class Usuario implements Serializable {

    private String nome = "";

    private String email = "";

    private String senha = "";

    private String cpf = "";

    private String rg = "";


    public Usuario() {

    }

    public Usuario(String email, String password) {
        this.email = email;
        this.senha = password;
    }


    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.senha = password;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }
}
