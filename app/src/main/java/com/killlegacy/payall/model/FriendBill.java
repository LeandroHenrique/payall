package com.killlegacy.payall.model;

import java.io.Serializable;

public class FriendBill implements Serializable  {

    private String email;

    private float value;


    public FriendBill(String email, float value) {
        this.email = email;
        this.value = value;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

}
