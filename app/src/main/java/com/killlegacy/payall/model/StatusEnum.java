package com.killlegacy.payall.model;

public enum StatusEnum {
    NEUTRAL, LOADING, SUCCESS, FAIL;
}
