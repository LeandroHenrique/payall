# hackathon
<br />

### Descrição
Aplicativo Android que funciona como uma carteira digital para integração com a API da Elo. A principal funcionalidade do aplicação é permitir que usuários possam dividir contas sem retirar o cartão do bolso. Tudo que o usuário precisa fazer é ler um QR Code do comerciante e adicionar os usuários para dividir a conta. Pronto, os usuários poderão dividir e escolher o quanto cada um irá pagar. <br />

Todos os dados do aplicativo são repassados para endpoints do pier onde é criado concentrado as informações e feita a integração com a API da ELO. <br />


### Tela de Login e Cadastro

A imagem à esquerda representa a tela de login onde o usuário poderá se autenticar na aplicação a partir do email e senha registrado. <br />
Caso o usuário ainda não seja cadastrado, ele poderá fazer um novo cadastro, através da nossa tela ou utilizando a API do facebook. <br />

A imagem à direita representa a tela de cadastro, o usuário poderá se registrar na API da elo através dos campos listados. <br />

<img src="screenshots/0.png" width="360" height="640" />
<img src="screenshots/1.png" width="360" height="640" />


<br />

### Tela de Home e Leitor de QR Code

A imagem à esquerda representa a tela Home da aplicação. Nela, o usuário poderá visualizar seus cartões cadastrados e selecionar a opção de ler QR Code de lugares como pizzaria, restaurantes e etc.<br />

A imagem à direita representa a tela de leitura de QR Code. Ao ler o QR code, o aplicativo terá acesso ao id do comerciante. Esse id será importante para que o backend possa saber o usuário da API que receberá o pagamento <br />

<img src="screenshots/2.png" width="360" height="640" />
<img src="screenshots/3.png" width="360" height="640" />


<br />

### Tela de Adição de Usuários para Compartilhar Conta e Tela de Tela de Resumo de Compra

A imagem à esquerda representa a tela que o usuário que leu o QR Code poderá determinar o item que será dividido e os amigos que irão dividir a conta. O usuário apenas precisa digital o email dos amigos que também estão cadastrado na API da Elo <br />

A imagem à direita representa a tela de confirmação de compra. Nela, o usuário poderá selecionar quanto cada amigo irá pagar. Por padrão, a conta é dividida igualmente entre todos os amigos participantes<br />

<img src="screenshots/4.png" width="360" height="640" />
<img src="screenshots/5.png" width="360" height="640" />


<br />

### Informações
      1. Para executar o projeto, basta importá-lo no Android Studios;
      2. @SomosTodosElo
	  
	  
![alt text](screenshots/6.png)
	  
	  
      
